import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { PagesRoutingModule } from './pages-routing.module';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PapadComponent } from './papad/papad.component';
import { PapadDetailsComponent } from './papad-details/papad-details.component';
import {DropdownModule} from 'primeng/dropdown';
import { AdminComponent } from './admin/admin/admin/admin.component';
import {TableModule} from 'primeng/table';
import { UsersComponent } from './users/users/users.component';
import { RecordComponent } from './record/record/record.component';
import { ChannelComponent } from './channel/channel/channel.component';
import { AdminDashboardComponent } from './admin_dashboard/admin-dashboard/admin-dashboard.component';
import { TagInputModule } from 'ngx-chips';
import { ImagePipe } from './image-valid.pipe';
import { RecordingTableComponent } from './recording-table/recording-table.component';
import { ChannelTableComponent } from './channel-table/channel-table.component';


@NgModule({
  declarations: [PapadComponent, PapadDetailsComponent, AdminComponent, UsersComponent, RecordComponent, ChannelComponent, AdminDashboardComponent, ImagePipe, RecordingTableComponent, ChannelTableComponent],
  imports: [
    CommonModule,
    TagInputModule,
    PagesRoutingModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true,
    }),
    ToastContainerModule,
    NgxAudioPlayerModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,TableModule,
  ]
})
export class PagesModule { }
