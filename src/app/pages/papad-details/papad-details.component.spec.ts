import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from '@angular/common/http';
import { PapadDetailsComponent } from './papad-details.component';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { LightboxModule } from 'ngx-lightbox';
import { Lightbox } from 'ngx-lightbox';
describe('PapadDetailsComponent', () => {
  let component: PapadDetailsComponent;
  let fixture: ComponentFixture<PapadDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PapadDetailsComponent ],
      imports: [
        RouterTestingModule,LightboxModule,
        HttpClientModule,ToastrModule.forRoot()
    ],
    providers: [
      ToastrService,Lightbox
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PapadDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
