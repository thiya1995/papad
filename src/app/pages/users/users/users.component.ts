import { Component, OnInit } from '@angular/core';
import { AdduserService } from '../../../services/admin/adduser.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userRole: any;
  userEmail: any;
  userName: any;
  constructor(private addUserService: AdduserService, private toaster: ToastrService,private activatedroute: ActivatedRoute,private router: Router ) { }

  ngOnInit(): void {
  }
  newUser() {
    let payload = {
      name: this.userName,
      email: this.userEmail,
      role: this.userRole
    }
    // this.addUserService.addNewUser(payload).subscribe(res => {
    //   this.toaster.success('Successfully Added New User');
    //   this.userName = '',
    //   this.userEmail = '',
    //   this.userRole = ''
    // })
  }
  userClick(){
    this.router.navigate(['pages/add/user']);
  }
  recordClick(){
    this.router.navigate(['pages/add/record']);
  }
  addChannel(){
    this.router.navigate(['pages/add/channel']);
  }
}
