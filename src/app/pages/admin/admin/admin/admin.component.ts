import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
users: any = [];
  constructor(private activatedroute: ActivatedRoute,private router: Router) { }

  ngOnInit(): void {
    this.users = [
      {name:"Thiya",id:1,email:"thiya@gmail.com"},
      {name:"Surya",id:2,email:"surya@gmail.com"},
      {name:"Deepak",id:3,email:"deepal@gmail.com"},
      {name:"Lilly",id:4,email:"lilly@gmail.com"},
      {name:"ramesh",id:5,email:"ramesh@gmail.com"}
    ]
  }
  userClick(){
    this.router.navigate(['pages/add/user']);
  }
  recordClick(){
    this.router.navigate(['pages/add/record']);
  }
  addChannel(){
    this.router.navigate(['pages/add/channel']);
  }
}
