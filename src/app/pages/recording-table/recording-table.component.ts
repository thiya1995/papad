import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { RecordService } from '../../services/admin/record.service';
import { StationsService } from '../../services/stations/stations.service';
import { AnthillService } from '../../services/anthill.service';
@Component({
  selector: 'app-recording-table',
  templateUrl: './recording-table.component.html',
  styleUrls: ['./recording-table.component.scss']
})

export class RecordingTableComponent implements OnInit {
  @Output() messageEvent = new EventEmitter<string>();
  recordData: any=[];
  tags:any = []
  constructor(private recordService: RecordService,private stationSer: StationsService,private anthilSer: AnthillService) { }

  ngOnInit(): void {
    this.getRecordData()
  }
  getRecordData(){
    this.anthilSer.getdata().subscribe(res => {
      this.recordData = res; 
      this.recordData.map(item => {
        this.tags = item.tags.split(',');
      })
      console.log(this.recordData)
    })
  }
  editRecord(event){
    this.messageEvent.emit(event)
  }

}
