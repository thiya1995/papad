import { Component, OnInit,Output, EventEmitter  } from '@angular/core';
import { StationsService } from '../../services/stations/stations.service';
@Component({
  selector: 'app-channel-table',
  templateUrl: './channel-table.component.html',
  styleUrls: ['./channel-table.component.scss']
})
export class ChannelTableComponent implements OnInit {
  @Output() messageEvent = new EventEmitter<string>();
  stationsData: any = [];
  constructor(private stationSer: StationsService) { }

  ngOnInit(): void {
    this.getStationsName()
  }
  getStationsName() {
    this.stationSer.getStationData().subscribe(res => {
      this.stationsData = res
    })}
    editChannel(event){
      this.messageEvent.emit(event)
    }
}
