import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule } from '@angular/common/http';
import { PapadComponent } from './papad.component';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { of } from 'rxjs';
import { AnthillService } from '../../services/anthill.service';


describe('PapadComponent', () => {
  let component: PapadComponent;
  let fixture: ComponentFixture<PapadComponent>;
  let service: AnthillService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PapadComponent ],
      imports: [
        RouterTestingModule,
        HttpClientModule,ToastrModule.forRoot()
    ],
    providers: [
      ToastrService,
    ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PapadComponent);
    service = TestBed.get(AnthillService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

it('should set recorddata property with the items returned from the server', () => {
  // Arrange - Setup
  const recordData =
  [
    {
      audio_url:"https://www.soundhelix.com/examples/mp3/SoundHelix-Song-5.mp3",
      id:1,
      img_tags:"http://www.simpleimageresizer.com/_uploads/photos/c0e675ae/Olipedia_logo.png",
      station:{
        id:1,
        station_image:"http://www.simpleimageresizer.com/_uploads/photos/c0e675ae/Olipedia_logo.png",
        station_name:"Olipedia"
      },
      tags:"music",
      upload_date:"2020-06-04T06:43:24.612Z"
    }
  ]
  
  spyOn(service, 'getdata').and.returnValue(of(recordData));
  component.ngOnInit();

  // spyOn(service, 'getProducts').and.returnValue(Observable.from([products]));

  // Act - Make the actual call


  // Assert - Check and report whether the test is pass or fail
  expect(component.recordData).toEqual((recordData));
});
});
