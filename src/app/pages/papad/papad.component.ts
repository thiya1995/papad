import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AnthillService } from '../../services/anthill.service';
import { AuthStore } from './../../authentication/auth-store';
import { ToastrService } from 'ngx-toastr';
import { StationsService  } from '../../services/stations/stations.service'


@Component({
  selector: 'app-papad',
  templateUrl: './papad.component.html',
  styleUrls: ['./papad.component.scss']
})
export class PapadComponent implements OnInit {
  recordData: any = [];
  changeTag: string;
  collectTags: any = [];
  imageUrl: any = [];
  tags: any = [];
  collectEle: any = [];
  selectedTag: any = [];
  deselectedArr: any = [];
  filterArr: any = [];
  cloneArray: any = [];
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private anthillService: AnthillService, public authStore: AuthStore,private toaster: ToastrService,private stationSer: StationsService) {

  }
  ngOnInit(): void {
    if(this.router.url === '/pages/papad;tags=' + this.activatedRoute.snapshot.params.tags){
      this.recordData = JSON.parse(sessionStorage.getItem('SelectedTag'));
      this.collectTags = JSON.parse(sessionStorage.getItem('Tags'));
      this.cloneArray = JSON.parse(sessionStorage.getItem('RecordData'))
    }
    if (this.router.url === '/pages/channel/' + this.activatedRoute.snapshot.params.event) {
      this.recordData = JSON.parse(sessionStorage.getItem('ChannelData'))
      this.collectTags = JSON.parse(sessionStorage.getItem('Tags'))
    }
    else if(this.router.url === '/pages/papad') {
      this.getRecordDatas();
      this.selectedTag = [];
      sessionStorage.setItem('SelectedTag',JSON.stringify(this.selectedTag));
    }
  }
  openTagModal(event) {
    // ******************** ROUTING FOR SUGGESTED TAG **************** //
    this.stationSer.stationData = {}
    this.router.navigate(['pages/papad-details', event.id])
  }
  onClickPlay(event) {
    // ******************** ROUTING FOR AUDIO TAG **************** //
    this.router.navigate(['pages/audio-details', event.id])
  }
  getRecordDatas() {
    let temp: any = []; //local array
    this.anthillService.getdata().subscribe(res => {
      this.recordData = res;
      this.recordData.map(element => {
        this.tags = element.tags.split(',').map(function(item) {
          return item.trim();
        });
        this.tags.map(key => {
          temp.push(key)
          
        })
        console.log(element);
        element.newTag = this.tags;

        this.imageUrl = element['img_tags'].split(',');

        element.firstImage = this.imageUrl[0]
        element.firstTag = this.tags[0]
        this.collectTags = temp.filter((a, b) => temp.indexOf(a) === b); // filtering the duplicated tags to store the unique tags in "collectTags"
        this.collectTags = this.collectTags.sort((a,b)=> a > b); // sort alphabetically
        // The first tag after sorting is a empty string,
        // removing that from the array
        this.collectTags.splice(0,1);
        
      });
      this.cloneArray = [...this.recordData]
      sessionStorage.setItem('RecordData', JSON.stringify(this.recordData));
      sessionStorage.setItem('Tags', JSON.stringify(this.collectTags));
    })
    
  }

  // ******************************   SELECTING TAG  *********************************** //

  selectList(ele, value) {
    var y = value.parentElement;
    //console.log(y);
    //y.style.backgroundColor = "#8395a7";
    
    var classList = y.classList;
    classList.toggle('suggest-selected');

    var x = value.nextSibling;
    this.collectEle.push(y);
    // this.selectedTag.push(ele)
    // x.addEventListener("click", function (e) {
      //   this.closeFunction()
      //   y.style.backgroundColor = '#ecf0f1';
      //   x.style.visibility = 'hidden';
      
      // }, false);
      this.collectTags.map((item) => {
        if (item === ele) {
          //x.style.visibility = 'visible';
        }
      });
      this.recordData = [];

    this.cloneArray.map(item => {
      item.newTag.forEach(key => {
        if (key === ele) {
          if(classList.contains("suggest-selected")){
            this.selectedTag.push(item);
            this.filterArr.push(ele);
            
          } else {
            var remIndx = this.selectedTag.findIndex(function(selItem){ 
              return selItem.id === item.id
            });
            this.filterArr.splice(this.filterArr.indexOf(ele), 1);
            this.selectedTag.splice(remIndx, 1);
            if(this.selectedTag.length == 0){
              this.selectedTag = [...this.cloneArray];
            }
            
            //console.log("remove tag", this.selectedTag, item)
          }
          
        }
      })
    })
    
    this.changeTag = this.filterArr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    if(this.changeTag) {
      
      this.router.navigate(['/pages/papad', { tags: this.changeTag }]); 
       
     } else {
       
       this.router.navigate(['/pages/papad', {}]);
     }
    
    this.recordData = this.selectedTag.filter((a, b) => this.selectedTag.indexOf(a) === b)
    sessionStorage.setItem('SelectedTag',JSON.stringify(this.recordData));
    this.toaster.success( this.recordData.length + ' audio(s) filtered')
  }
  /*closeFunction(x) {
    // THIS FUNCTION MIGHT NOT BE NEEDED
    let innerText = x.parentElement.innerText
    this.collectEle.map(ele => {
      if (ele.lastElementChild === x) {
        //ele.style.backgroundColor = '#007bff';
        ele.classList.toggle('suggest-selected');
        x.style.visibility = 'hidden'
      }
    })
    this.recordData = [];
    this.deselectedArr.push(innerText)

    let dummyFilter = [];
    this.filterArr = this.filterArr.filter(u => this.deselectedArr.findIndex(lu => lu === u) === -1);
    this.changeTag = this.filterArr.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    this.router.navigate(['/pages/papad', { tags: this.changeTag }]);
    this.cloneArray.map(item => {
      item.newTag.map(ele => {
        this.filterArr.map(key => {
          if (key === ele) {
            dummyFilter.push(item)
          }
        })
      })
    })
    this.selectedTag = [];
    
    this.recordData = dummyFilter.filter((a, b) => dummyFilter.indexOf(a) === b)
    this.selectedTag = [...this.recordData];
    sessionStorage.setItem('SelectedTag', JSON.stringify(this.selectedTag))
    sessionStorage.setItem('DeselectedTag', JSON.stringify(this.recordData));
    if ((JSON.parse(sessionStorage.getItem('DeselectedTag'))).length === 0) {
      this.recordData = JSON.parse(sessionStorage.getItem('RecordData'))
    }
  }
  openRightMenu() {
    document.getElementById("rightMenu").style.display = "block";
  }

  closeRightMenu() {
    document.getElementById("rightMenu").style.display = "none";
  }*/
}

