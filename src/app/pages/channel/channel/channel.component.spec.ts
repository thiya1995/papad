import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientModule ,HttpEvent} from '@angular/common/http';
import { ChannelComponent } from './channel.component';
import { RecordService } from '../../../services/admin/record.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import {Observable } from 'rxjs';
import 'rxjs/Rx';
import { DebugElement } from '@angular/core';

describe('ChannelComponent', () => {
  let component: ChannelComponent;
  let fixture: ComponentFixture<ChannelComponent>;
  let service: RecordService;
  let de: DebugElement;
  let el : HTMLElement;
  const channelData = {
    station_image:'Janastu',
    station_name	:'JanastuimageUrl'
  }
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelComponent ],
      imports: [
        RouterTestingModule,
        HttpClientModule,FormsModule,ReactiveFormsModule
    ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelComponent);
    service = TestBed.get(RecordService);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
    fixture.detectChanges();
  });
  it('form should be valid',() => {
     component.ChannelForm.controls['channelName'].setValue('Janastu');
     component.ChannelForm.controls['imageUrl'].setValue('JanastuimageUrl');
     
  });
  // it(
  //   'should show product details for a particular product',
  //   () => {
      
  //     fixture.detectChanges();

  //     fixture.whenStable().then(() => {
  //       const nameElement: HTMLInputElement = fixture.debugElement.query(
  //         By.css('#channelName')
  //       ).nativeElement;
  //       const imageElement: HTMLInputElement = fixture.debugElement.query(
  //         By.css('#localUrl')
  //       ).nativeElement;
  //       component.ChannelForm.controls['channelName'].setValue('Janastu');
  //       component.ChannelForm.controls['imageUrl'].setValue('JanastuimageUrl');
  //       expect(nameElement.value).toContain(channelData.station_name);
  //       expect(imageElement.value).toContain(channelData.station_image);
  //     });
  //   }
  // );
  it('should save product details when form is submitted', () => {
    
    component.submitted = true;
    const spy = spyOn(service, 'addChannel').and.returnValue(Observable.empty()
    );
    const form = fixture.debugElement.query(By.css('form'));
    form.triggerEventHandler('submit', null);
   
    expect(spy).toHaveBeenCalled();
    // const button = fixture.debugElement.query(By.css('#save'));
    // button.nativeElement.click();
  });
  
});
