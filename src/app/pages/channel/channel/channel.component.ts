import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RecordService } from '../../../services/admin/record.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { StationsService } from '../../../services/stations/stations.service';
@Component({
  selector: 'app-channel',
  templateUrl: './channel.component.html',
  styleUrls: ['./channel.component.scss']
})
export class ChannelComponent implements OnInit {
  imageUrl: string;
  channelName:string;
  ChannelForm: FormGroup;
  submitted: boolean = false;
  stationsData: any;
  add: boolean = true;
  update: boolean = false;
  constructor(private router: Router,private recordService: RecordService, private fb: FormBuilder,private toaster: ToastrService,private stationSer: StationsService ) { }

  ngOnInit(): void {
    this.ChannelForm = this.fb.group({
      channelName: ['', Validators.required],
      imageUrl: ['', Validators.required],
    });
  
  }
  userClick(){
    this.router.navigate(['pages/add/user']);
  }
  recordClick(){
    this.router.navigate(['pages/add/record']);
  }
  addChannel(){
    this.router.navigate(['pages/add/channel']);
  }
  addChannelData(){
    this.submitted = true;
    this.add = true;
      let payload = {
      station_image:this.ChannelForm.value.imageUrl,
      station_name:this.ChannelForm.value.channelName
    }
    this.recordService.addChannel(payload).subscribe(res => {
      this.toaster.success("Successfully added");
      this.imageUrl = ""; 
      this.channelName = "";  
    })
  }

  receiveMessage(event){
      this.add = false;
      this.update = true;
      this.imageUrl = event.station_image; 
      this.channelName = event.station_name;
    }
}
