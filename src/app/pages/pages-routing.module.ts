import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PapadComponent } from './papad/papad.component';
import { PapadDetailsComponent } from './papad-details/papad-details.component';
import { AdminComponent } from './admin/admin/admin/admin.component';
import { UsersComponent } from './users/users/users.component';
import { RecordComponent } from './record/record/record.component';
import { ChannelComponent } from './channel/channel/channel.component';
import { AdminDashboardComponent } from './admin_dashboard/admin-dashboard/admin-dashboard.component';


const routes: Routes = [
  { path: '', redirectTo: 'papad', pathMatch: "full" },
  { path: 'papad', component: PapadComponent },
  { path: 'papad-details/:event', component: PapadDetailsComponent },
  { path: 'audio-details/:event', component: PapadDetailsComponent },
  { path: 'channel/:event', component: PapadComponent },
  { path: 'admin/dashboard', component: AdminDashboardComponent},
  { path: 'admin', component: ChannelComponent },
  { path: 'add/user', component: UsersComponent },
  { path: 'add/record', component: RecordComponent },
  { path: 'add/channel', component: ChannelComponent },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
