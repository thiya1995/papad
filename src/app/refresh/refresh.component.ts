import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RouterModule, Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-refresh',
  templateUrl: './refresh.component.html',
  styleUrls: ['./refresh.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RefreshComponent implements OnInit {

  urlToChange: any;
  constructor(private router: Router,
    private activeroute: ActivatedRoute) { 
      console.log('snapshot trace');
      console.log(this.activeroute.snapshot);
  }

  ngOnInit() {
    // this.changePage();
    console.log('snapshot trace');
    console.log(this.activeroute.snapshot);
  }
}