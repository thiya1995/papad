import { OnInit, Component } from '@angular/core';
import { AuthStore } from './authentication/auth-store'
import { Observable, BehaviorSubject, of } from 'rxjs';
import { IsLoadingService } from '@service-work/is-loading';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLoading: Observable<boolean>;

  title = 'papad';
  constructor(private isLoadingService: IsLoadingService,
      private authstore: AuthStore, private router: Router,){
    
  }

  ngOnInit(): void {
    this.isLoading = this.isLoadingService.isLoading$();

  }
}
