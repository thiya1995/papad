import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CloudSyncService } from '../services/cloud-sync.service';
import * as _ from 'lodash';
import { Annotation } from '../models/annotation';
import { AnthillService } from '../services/anthill.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-sync-component',
  templateUrl: './sync-component.component.html',
  styleUrls: ['./sync-component.component.scss']
})
export class SyncComponentComponent implements OnInit {
  private filesUrl: string = environment.files;

  constructor(private _syncService: CloudSyncService,
  			  private _annoService: AnthillService) { }

  ngOnInit(): void {
  	this.getData();
  	this.getFolderStats();
  	// fetch data from service
  	this._annoService.getdata().subscribe(res => {
  	  this.annotations = res;
  	  console.log(this.annotations, "annotations");
  	});
  }
  config: any[] = ["papad-send", "papad-receive", "papad-send-receive"];
  folders: any[] = []; //["papad-send", "papad-receive", "papad-send-receive"];
  subfolders: any[] = [];
  folderIndex: any[] = [];
  defaultFolderIndex: any[] = [];
  defaultFolder: any;
  allFolders: any[] = []; 
  openedFolder: string;
  openFolderContents: any;
  viewerState: boolean;
  viewerUrl: string;
  viewerMediaType: string;
  annotations: Annotation[];
  syncState: any[];
  addingAnno: boolean = false;
  openAnnoContainer: boolean = false;

  @ViewChild('videoPlayer') videoplayer: ElementRef;

	getData() {
		// Test syncthing rest
		this._syncService.getdata().subscribe(res => {
			console.log(res, "sync get data");
			this.folders = Object.keys(res);
			/*
			this.folders.push(res[this.config[0]]);
			this.folders.push(res[this.config[1]]);
			this.folders.push(res[this.config[2]]);
			*/
			this.defaultFolder = res;
			this.parseKeys();
		});

	 }

	 parseKeys() {
	 	this.folderIndex = _.map(this.folders, (folder => {
	 		return folder;
	 	}));
	 	this.defaultFolderIndex = _.toPairs(this.defaultFolder);
	 	console.log(_.toPairs(this.defaultFolder), this.defaultFolderIndex, this.folderIndex, "index in syncthing default folder");
	 }
	 hasSubfolder(obj) {
	 	let evaluate = obj && (Object.keys(obj).length === 0);
	 	console.log(obj, evaluate, "is empty")
	 	return !evaluate;
	 }
	 getSubfolders(obj) {
	 	return Object.keys(obj);
	 }
	 isFile(obj) {
	 	return Array.isArray(obj[1]);
	 }
	 getFolderStats() {
	 	this._syncService.getdata().subscribe(res => {
	 		console.log(res, "sync folder stats");
	 		this.syncState = res;
	 		this.allFolders = Object.keys(res);
	 	});
	 }
	 getFileCount(folder) {
	 	var files = this.syncState[folder];
	 	return Object.keys(files).length;
	 }
	 openFolder(e) {
	 	console.log(e.target.dataset["folder"], "clicked folder");
	 	this.openedFolder = e.target.dataset["folder"];
	 	this._syncService.browseFolder(this.openedFolder).subscribe(res => {
	 		console.log(res, _.toPairs(res), "Folder contents");
	 		this.openFolderContents = _.toPairs(res);

	 	});
	 }

	 getRemoteUrl(folder, name) {
	 	console.log(folder, name);
	 	if(!this.isFile(folder)) {
	 		//return "https://syncshare.janastu.org/"+folder[0]+'/'+name;
	 		return this.filesUrl+folder[0]+'/'+name;
	 	} else {
	 		//return "https://syncshare.janastu.org/"+this.openedFolder+'/'+name;
	 		return this.filesUrl+this.openedFolder+'/'+name;
	 	}
	 	
	 	
	 }

	 getSyncthingPath(folder, name) {
	 	if(!this.isFile(folder)) {
	 		//return "https://syncshare.janastu.org/"+folder[0]+'/'+name;
	 		return folder[0]+'/'+name;
	 	} else {
	 		//return "https://syncshare.janastu.org/"+this.openedFolder+'/'+name;
	 		return this.openedFolder+'/'+name;
	 	}
	 }
	 hasAnno(folder, name) {
	 	// check if ingest anno exists for this target
	 	// return css class
	 	console.log(folder, name, this.annotations, "target")
	 	var target = this.getRemoteUrl(folder, name)
	 	var parts = target.split('/');
	 	var filename = parts[parts.length - 1];
	 	var remoteParts, remoteFilename;
	 	
	 	var annosForTarget = this.annotations.filter(function(item){
	 		if(item.contentUrl){
	 			remoteParts = item.contentUrl.split('/');
	 			remoteFilename = remoteParts[remoteParts.length - 1];
	 			return remoteFilename === filename;
	 		} else {
	 			remoteParts = item.audio_url.split('/');
	 			remoteFilename = remoteParts[remoteParts.length - 1];		
	 			return remoteFilename === filename;
	 		}
	 		
	 	});
	 	console.log(annosForTarget, "annotations for target")
	 	if(annosForTarget.length){
	 		return "ingested"
	 	} else {
	 		return "not-ingested"
	 	}

	 }

	 openViewer(e) {
	 	console.log(e.target.dataset, "viewer url");
	 	this.viewerState = true;
	 	this.viewerUrl = this.filesUrl + e.target.dataset["url"];
	 	let parts = this.viewerUrl.split('/');
	 	this.viewerMediaType = parts[(parts.length-1)].slice(-3).toLowerCase();
	 	this.changeVideo();
	 }

	 changeVideo() {
	 	this.videoplayer.nativeElement.src = this.viewerUrl;
	 }
	 toggleVideo(event: any) {
	     this.videoplayer.nativeElement.play();
	 }
	 openIngestForm(e) {
	 	this.addingAnno = !this.addingAnno;
	 }
	 showAnnoContainer(e) {
	 	this.openAnnoContainer = !this.openAnnoContainer;
	 }
	 lengthOfAnnosForTarget(target) {
	 	var annosForTarget = this.annotations.filter(function(anno){
	 		if(anno.contentUrl){
	 			return anno.contentUrl == target;
	 		} else if (anno.audio_url){
	 			return anno.audio_url == target
	 		} else {
	 			console.log(anno, "no audio url")
	 		}
	 	})
	 	return annosForTarget.length;
	 }
	 IngestSubmitted(e){
	 	console.log(e, "suny knowing ingest submit")
	 }
}
