import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SyncComponentComponent } from './sync-component.component';

describe('SyncComponentComponent', () => {
  let component: SyncComponentComponent;
  let fixture: ComponentFixture<SyncComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SyncComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SyncComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
