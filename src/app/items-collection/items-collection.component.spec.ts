import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsCollectionComponent } from './items-collection.component';

describe('ItemsCollectionComponent', () => {
  let component: ItemsCollectionComponent;
  let fixture: ComponentFixture<ItemsCollectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsCollectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsCollectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
