import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnoFormComponent } from './anno-form.component';

describe('AnnoFormComponent', () => {
  let component: AnnoFormComponent;
  let fixture: ComponentFixture<AnnoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
