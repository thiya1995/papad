import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment'
import * as _ from 'lodash';
@Injectable({
  providedIn: 'root'
})

export class StationsService {
  private baseUrl: string = environment.apiUrl.stations.base_stationUrl;
  public stationData = {};
  constructor(private _http: HttpClient) { }

  getStationData(): Observable<any> {
    return this._http.get(this.baseUrl)
      .pipe(map((response: Response) => response),
        catchError(this.handleError)
      )
  }
  getStationsById(id):Observable<any>{
    return this._http.get(this.baseUrl  +id).pipe(map((response: Response) => response),
    catchError(this.handleError))
  }

  
  private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'ChartService error');
  }
}
