import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecordService {
  private baseUrl: string = environment.apiUrl.postAnnos.base_recordUrl+'recordings/';
  private channelUrl: string = environment.apiUrl.stations.base_stationUrl;
  private fragmentUrl: string = environment.apiUrl.postAnnos.base_recordUrl+'fragments/';
  constructor(private _http: HttpClient) { }

  addRecords(data): Observable<any> {
    return this._http.post(this.baseUrl, data).pipe(
      map((response: Response) => response),
      catchError(this.handleError)
    )
  }
  addChannel(data): Observable<any> {
    return this._http.post(this.channelUrl, data).pipe(
      map((response: Response) => response),
      catchError(this.handleError)
    )
  }
  addFragment(data): Observable<any> {
    return this._http.post(this.fragmentUrl, data).pipe(
      map((response: Response) => response),
      catchError(this.handleError)
    )
  }
  private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'ChartService error');
  }
}
