import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdduserService {
  // private baseUrl: string = environment.apiUrl.users.base_userUrl
  constructor(private _http: HttpClient) { }
  // addNewUser(data):Observable<any> {
  //   return this._http.post(this.baseUrl , data) .pipe(
  //     map((response : Response) => response),
  //     catchError(this.handleError)
  //   )
  // }
  private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'ChartService error');
  }
}

