import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CloudSyncService {
  // rest/db/browse?folder=default&levels=1
  private baseUrl: string = environment.syncthing.url; //"http://192.168.83.168:8384/";
  private apiKey: string = environment.syncthing.key; //"UCghtfhjGsczJo9Q4EYcmqKuWioomtK6";

  constructor(private _http: HttpClient) { }
  getBaseUrl() {  
        return this.baseUrl;  
  } 
   // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'X-API-Key': this.apiKey
    })
  }


   getdata(): Observable<any> {
   	return this._http.get(this.baseUrl+'rest/db/browse?folder=default', this.httpOptions)
   	  .pipe(map((response: Response) => response),
   	    catchError(this.handleError)
   	  )
   }

   browseFolder(folderName): Observable<any> {
     return this._http.get(this.baseUrl+'rest/db/browse?folder=default&prefix='+folderName, this.httpOptions)
       .pipe(map((response: Response) => response),
         catchError(this.handleError)
       )
   }

   getFolderStats(): Observable<any> {
     return this._http.get(this.baseUrl+'rest/stats/folder', this.httpOptions)
       .pipe(map((response: Response) => response),
         catchError(this.handleError)
       )
   }
   private handleError(error) {
    console.error(error);
    return Observable.throw(error || 'Record Service error');
  }
}
