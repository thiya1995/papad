import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthStore } from './auth-store';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { PapadComponent } from '../pages/papad/papad.component';
import { PapadDetailsComponent } from '../pages/papad-details/papad-details.component'
describe('AuthStore', () => {
  let service: AuthStore;

  beforeEach(() => {
    TestBed.configureTestingModule({
        declarations: [ PapadDetailsComponent,PapadComponent ],
        imports: [
            RouterTestingModule,
            HttpClientModule,
            HttpClientTestingModule
        ],
    });
    service = TestBed.inject(AuthStore);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
