import { Injectable, Input } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { AnthillService } from '../services/anthill.service'

@Injectable({
  providedIn: 'root'
})
export class AuthStore {
    public recordData: any = [];
    public responseData: any = [];
    constructor(public anthillService: AnthillService){
        if (sessionStorage.getItem("RecordData")) {
            this.recordData = JSON.parse(sessionStorage.getItem('RecordData'));
          }
          if (sessionStorage.getItem("ResponseData")) {
            this.responseData = JSON.parse(sessionStorage.getItem('ResponseData'));
          }
          this.getFullData()
        }
        getFullData(){
                  this.anthillService.getdata().subscribe(res => {
                  this.responseData = res;
                  sessionStorage.setItem('ResponseData',JSON.stringify(this.responseData));
                  })
              }
              ReloadRecordData() {
                if (sessionStorage.getItem("RecordData")) {
                  this.recordData = JSON.parse(sessionStorage.getItem("RecordData"));
                  console.log('qw',this.recordData)
                }
              }
}

