import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelDropdownComponent } from './channel-dropdown.component';

describe('ChannelDropdownComponent', () => {
  let component: ChannelDropdownComponent;
  let fixture: ComponentFixture<ChannelDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
