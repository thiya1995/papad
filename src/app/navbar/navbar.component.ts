import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { StationsService } from '../services/stations/stations.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  logoUrl: any;
  stationsData: any;
  channel: string = environment.deviceId;
  
  @Output() SendValue = new EventEmitter<string>();
  constructor(public apiService: StationsService, private router: Router) { }

  ngOnInit(): void {
    this.logoUrl = environment.logo.imageUrl;
    this.getStationsName();
  }
  getStationsName() {
    this.apiService.getStationData().subscribe(res => {
      console.log(res, "stations data");
      this.stationsData = res.map((ele) => ({
        label: ele.station_name,
        value: ele,
        image: ele.station_image
      }));
      this.stationsData.map(ele => {
        console.log(ele);
        if(typeof ele.label !== 'string') return ''
        ele.label = ele.label.charAt(0).toUpperCase() + ele.label.slice(1);
      });
    });
  }
  selectChannel(event) {
    // this.apiService.getStattionsById(event.id).subscribe(res => {
    // })
    console.log(event);
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
      this.router.navigate(['pages/channel', event.id]));

  }
  
  refreshPage(){
   
}
}