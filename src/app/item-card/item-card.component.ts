import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-item-card',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  @Input() id: string;
  @Input() imageTags: string; // = "//papad.test.openrun.net/assets/images/logo-transparent.png";
  @Input() tags: string; // = "Addition,Basic";
  @Input() headerText: string; // = "file name or identifier";
  tagsList:string[]; //Array of tags
  cardImage: string;
  
  ngOnInit(): void {
    if(this.imageTags) {
      this.cardImage = this.imageTags.split(',')[0];
    }
  	
    if(this.tags) {
      if(this.tags.indexOf(',') > -1) this.tagsList = this.tags.split(',');
      else this.tagsList = this.tags.split(" ");

      if(this.tagsList.indexOf("tags") > -1){
        this.tagsList.splice(this.tagsList.indexOf("tags"), 1);
      }
    }


  }
  gotToDetailPage(e, item){
    console.log(e, this.id);
  }
}
