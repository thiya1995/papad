import { Component, 
         OnInit, ViewChild, 
         ElementRef, AfterViewInit, HostListener  } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AnthillService } from '../services/anthill.service';
import { Fragment } from '../models/fragment';
import { RecordService } from '../services/admin/record.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../environments/environment'
import { FileUploadValidators, FileUploadControl } from '@iplab/ngx-file-upload';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Options } from '@angular-slider/ngx-slider';
import { IsLoadingService } from "@service-work/is-loading";


@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {
  @ViewChild('closeUpload') closeUploadModal: ElementRef;
  constructor(
    private _http: HttpClient, private isLoadingService: IsLoadingService,
  	private route: ActivatedRoute,
    private recordService: RecordService,
  	private _apiservice: AnthillService,
    private toaster: ToastrService) { }
    private filesControl = new FormControl(null, FileUploadValidators.filesLimit(1));
    public fileUrl: string = environment.files;
    public uploadForm = new FormGroup({
      startTime: new FormControl('Start Time', Validators.required),
      endTime: new FormControl('End Time', Validators.required),
      userTags: new FormControl('Tags', Validators.required),
      file: this.filesControl,
      textArea: new FormControl('Comment', Validators.required)
  });
  previewUrl:any = null;
  private uploadUrl: string = environment.uploadUrl;
  public multipleFiles: boolean = false;
  id: string = this.route.snapshot.paramMap.get('itemId');
  annotation: any;
  viewerUrl: string;
  mediaType: string;
  textArea: string;
  @ViewChild('player') player: ElementRef;
  @ViewChild('timeline') bubbles: ElementRef;
  @ViewChild('fragmentsContainer') fragmentContainer: ElementRef;
  @ViewChild('listViewContainer') listViewContainer: ElementRef;
  @ViewChild('playerSlider') playerSlider: ElementRef;
  //@ViewChild('player') player: ElementRef;
  fragmentModel: Fragment; 
  fragments: Fragment[];
  userTags: string;
  imageTag: string;
  userFragment: string;
  startTime: number = 0.0;
  endTime: number;
  duration: number;
  isListView: boolean = false;
  isPaused: boolean = true;
  playerCurrentTime: number = 0;
  playerSliderOptions: Options = {
        floor: 0,
        ceil: 0,
        step: 0.1,
        showTicks:true,
        ticksTooltip: (v: number): string => {
              return 'Tooltip for ' + v;
            }
      };
  volumeSliderOptions: Options = {
    floor: 0,
    ceil: 100,
    step: 1
  };
  volume: number = 100;
  //allowedAudioTypes: 

  setValues() {
    var filename; //parts, path;
  	if(this.annotation.contentUrl){
      filename = this.annotation.contentUrl;//.split('/');
      //path = parts[parts.length-2]+'/'+parts[parts.length-1];
     
  		this.viewerUrl = environment.files+filename;
      console.log(this.viewerUrl , environment, filename, "viewer path");
  	} else {
  		this.viewerUrl = this.annotation.audio_url
  	}
  	if(!this.annotation.type){
  		let parts = this.viewerUrl.split('/')
  		this.mediaType = parts[(parts.length-1)].split('.')[1] || "";
      console.log(this.mediaType, "mediaType");

  	} else {
  		this.mediaType = this.annotation.type;
  	}
    
    
  	
  }
  toggleListView(e){
    this.isListView = !this.isListView;
  }
  textOnChange(e) {
  	console.log(e, this.textArea, "text area change");
  }

  submit() {
    console.log(this.uploadForm.getRawValue(), "check form value");
    if(this.uploadForm.getRawValue().file){
      const formData = new FormData();  		
  		formData.append('file', this.uploadForm.getRawValue().file[0]);
  		formData.append('name', this.uploadForm.getRawValue().file[0].name);
      this.isLoadingService.add({ key: ["default", "single"] });
  		this._http.post<any>(this.uploadUrl, formData).subscribe(
  			(res) => {
  				
          this.sendFragment(res.url);			
  			},
  			(err) => console.log(err, "error")
  			//this.toaster.success('Successfully uploaded File');
  		);
    } else {
      this.sendFragment("");
    }
    

  }
  sendFragment(targetUrl){
    var currentTime = this.player.nativeElement.currentTime.toFixed(1);      
    
    var userAddedFragment = this.userFragment;
    var message;
    let filename = targetUrl.split('/').slice(-1)[0];
    var body = {
      tags:  this.userTags, 
      imgTags: filename, 
      text: this.textArea, 
      purpose: "tagging",
      station_name: environment.deviceId
    };
    // TODO: need to test the url patterns for new version
    var target = {
      id: this.viewerUrl+"#t="+this.startTime+","+this.endTime,
      format: this.annotation.type,
      src: this.annotation.contentUrl
    };
    var selector = {
      value: "t="+this.startTime+","+this.endTime, 
      type: "FragmentSelector",
      conformsTo: "http://www.w3.org/TR/media-frags/"
    };
    this.fragmentModel = new Fragment(
    target,
    body,
    selector,
    'anonymous cat'
    );
  
    console.log(this.fragmentModel, Number(this.startTime) < Number(this.endTime), "fragment");
    if(Number(this.startTime) < Number(this.endTime) && Number(this.endTime) <= Number(this.player.nativeElement.duration)){
      this.recordService.addFragment(this.fragmentModel).subscribe(res => {
        this.isLoadingService.remove({ key: ["default", "single"] })
        this.toaster.success('Successfully Added New Fragments');
        //this.fragments.push(this.fragmentModel);
        this.fragments = [...this.fragments, this.fragmentModel];
        //this.reset();
        this.uploadForm.reset();
        //this.submitted = !this.submitted;
        //this.onSubmitted.emit(true);
        this.reset();
      });
    } else {
      console.log(this.toaster, this.startTime, this.endTime, "toaster object");


      if(Number(this.startTime) > Number(this.endTime)) message = "The end time should be greater than start time "+this.startTime+"s";

      if(Number(this.endTime) > Number(this.player.nativeElement.duration)) message = "The end time should be less than "+ this.player.nativeElement.duration +"s - the overall duration";

      this.toaster.error(message, "ERROR")
    }
    
  	console.log(this.player.nativeElement.currentTime, this.fragmentModel, "video player");
  	// check for mediaType, if AV, then duration part of fragment
  	// image  - no fragment, whole obj annotation
  	// clear after adding - show the added obj
  }
  preview() {
    // Show preview 
    var mimeType = this.uploadForm.getRawValue().file[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    var reader = new FileReader();      
    reader.readAsDataURL(this.uploadForm.getRawValue().file[0]); 
    reader.onload = (_event) => { 
      this.previewUrl = reader.result;
    }
}
  getCurrentTime() {
    return this.player.nativeElement.currentTime.toFixed(1);
  }
  getDuration() {
    return this.player.nativeElement.duration;
  }
  reset() {
    this.uploadForm.getRawValue()['userTags'] = "";
    this.uploadForm.getRawValue()['textArea'] = "";
    this.uploadForm.getRawValue()['imageTag'] = "";
    this.uploadForm.getRawValue()['startTime'] = this.getCurrentTime();
    this.uploadForm.getRawValue()['endTime'] = this.getCurrentTime()+5;
  }
  toggleVideo(e) {
    console.log(this.player, "video pplayer");
    this.player.nativeElement.play();
  }
  renderFragment() {
    // fragment related state
  }
  formatFragmentTags(tags) {
    if(tags){
      if(tags.indexOf(',') > -1) return tags.split(',');
      else return tags.split(' ');
    }
  }
  isImage(urlString) {
    let url;
      try {
        url = new URL(urlString);
        
      } catch (_) {
        return false;  
      }
      return true;
  }
  playFragment(e, index) {
    console.log(e, index, this.bubbles, this.listViewContainer, "play frag")
    this.isPaused = false;
    var startTime = this.getStartFrag(e);
    var endTime = (this.getEndFrag(e) - startTime) * 1000;
    this.player.nativeElement.currentTime = startTime;
    // TODO: the DOM state update - should find a better way to do this.
    if(this.bubbles) this.bubbles.nativeElement.children[index].style.backgroundColor="cyan";
    if(this.bubbles) this.bubbles.nativeElement.children[index].style.opacity="0.7";
    if(this.fragmentContainer) this.fragmentContainer.nativeElement.children[index].style.backgroundColor="#cddff4";
    if(this.listViewContainer) this.listViewContainer.nativeElement.children[index].style.backgroundColor="#cddff4";

    if(this.player.nativeElement.paused) this.player.nativeElement.play();

    var that = this;

    var pauseAfterPlaying = setInterval(function(){
      console.log("paused");
      that.player.nativeElement.pause();
      if(that.bubbles) that.bubbles.nativeElement.children[index].style.backgroundColor="#1967D2";
      if(that.bubbles) that.bubbles.nativeElement.children[index].style.opacity="0.5";

      if(that.fragmentContainer) that.fragmentContainer.nativeElement.children[index].style.backgroundColor="#f0f6fd";
      if(that.listViewContainer) that.listViewContainer.nativeElement.children[index].style.backgroundColor="#f0f6fd";

      that.pauseAfter(pauseAfterPlaying);
    }, endTime);
  }
  
  getStartFrag(val) {
    return Number(val.split('=')[1].split(",")[0]);
  }
  getEndFrag(val) {
    return Number(val.split('=')[1].split(",")[1]);
  }
  secondsToReadable(secs) {
    var seconds = secs;
    
    var numhours = Math.floor(((seconds % 31536000) % 86400) / 3600);
    var numminutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
    var numseconds = Math.floor(((seconds % 31536000) % 86400) % 3600) % 60;
    //check if minutes < 10 to format the minutes as 2 digits
    var finalString = seconds < 599 ? numhours + ":0" + numminutes + ":" + numseconds : 
           numhours + ":" + numminutes + ":" + numseconds;
    return finalString;
   
  }

  getLeftOffset(fragment){
    //console.log(fragment, "from left offset");
    var timeSegment = Number(fragment.split('=')[1].split(',')[0]);
    var offset = timeSegment * 100 / this.player.nativeElement.duration;   
    return offset + '%';
  }

  getBubbleWidth(fragment) {
    var timeSegment = fragment.split('=')[1].split(',');
    // || condition in case difference is 0 this. constant 0.3 needs to 
    // be compensated in the left offset value
    var difference = Math.round(timeSegment[1]) - Math.round(timeSegment [0]) || 0.3;
    return (difference * 100) / this.player.nativeElement.duration + '%';
  }

  bubbleOnHover(e) {
    console.log(e, "on hover")
  }

  setStartTime() {
    this.startTime = this.getCurrentTime();
  }

  setEndTime() {
    this.endTime = this.getCurrentTime();
  }

  playIntro() {
    this.isPaused = false;
    this.player.nativeElement.currentTime = this.startTime;
    if(this.player.nativeElement.paused) this.player.nativeElement.play();
    
    var that = this;
    var delayFunc = setInterval(function(){
      that.player.nativeElement.currentTime = that.startTime;
      that.player.nativeElement.pause();

      that.pauseAfter(delayFunc);
    }, 2000);
  }

  playOutro() {
    this.isPaused = false;
    this.player.nativeElement.currentTime = this.endTime - 2;
    if(this.player.nativeElement.paused) this.player.nativeElement.play();

    var that = this;
    
    var delayFunc = setInterval(function(){
      that.player.nativeElement.pause();
      that.pauseAfter(delayFunc);
    }, 2000);
  }

  previewFrag(){
    this.isPaused = false;
    console.log("Preview", this.startTime, this.endTime)
    this.player.nativeElement.currentTime = this.startTime;
    if(this.player.nativeElement.paused) this.player.nativeElement.play();

    var that = this;
    var endDelay = Number(this.endTime - this.startTime) * 1000;
    var pauseAfter = setInterval(function(){
      console.log("paused");
      that.player.nativeElement.pause();
      that.pauseAfter(pauseAfter);
    }, endDelay);

  }

  pauseAfter(delayFunc){
    this.isPaused = true;
    console.log(delayFunc, "delay");
    clearInterval(delayFunc);
    delayFunc = null;
    return;
  }

  /* Player controls */
  @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) { 
      //this.key = event.key;
      console.log(event.key, event);
      switch(event.key) {
        case "Escape":
          this.playMedia();
          break;
        case "ArrowRight":
          this.forward();
          break;
        case "ArrowLeft":
          this.rewind();
          break;
        case "+":
          this.volume += 5;
          break;
        case "-":
          this.volume -= 5;
          break;
        default:
          console.log("do nothing");
      }

    }

  playMedia() {
    var $player = this.player.nativeElement;
    if($player.paused) {
      $player.play();
      this.isPaused = false;
    } else {
      $player.pause();
      this.isPaused = true;
    }
    
  }
  rewind() {
    this.player.nativeElement.currentTime -= 2.5;
  }
  fastRewind() {
    this.player.nativeElement.currentTime -= 5;
  }
  forward() {
    this.player.nativeElement.currentTime += 2.5;
  }
  fastForward() {
    this.player.nativeElement.currentTime += 5;
  }
  seekedSlide(e) {
    console.log(e, "input slide event");
    this.player.nativeElement.currentTime = Number(e.value);
  }
  getSliderPos(){
     return this.playerSlider.nativeElement.value;
  }
  changeVol(e) {
    //UserChange event sends a event object
    // but valueChange just sends a number
    // conditional check to handle both
    if(e.value) this.player.nativeElement.volume = e.value/100;
    else this.player.nativeElement.volume = e/100
  }
  ngAfterViewInit(): void {
    console.log(this.player, "video pplayer");
    let that = this;
    that.closeUploadModal.nativeElement.click();
    that.player.nativeElement.addEventListener('seeked', (event) => {
   
      /*let roundedCurrentTime = Math.round(that.getCurrentTime() * 10) / 10;
      that.startTime = roundedCurrentTime;
      that.endTime =  roundedCurrentTime + 5;*/
      console.log(event, "seeked event");
    });

    that.player.nativeElement.addEventListener("timeupdate", (event) => {
      that.playerCurrentTime = that.player.nativeElement.currentTime;
    });

    that.player.nativeElement.addEventListener("canplay", (event) => {
      that.duration = Math.round(that.player.nativeElement.duration);
      that.playerCurrentTime = that.player.nativeElement.currentTime;
      if(that.duration < 175) {
        that.playerSliderOptions = {
          floor: 0,
          ceil: that.duration,
          showTicks:true,
          step: 0.5
        }
      } else {
        that.playerSliderOptions = {
          floor: 0,
          ceil: that.duration,
          showTicks: true,
          step: 0.5,
          tickStep: 5
        }
      }
      
    });

    that.startTime = that.getCurrentTime();
    console.log(that.getCurrentTime(), this.bubbles, "current TIme");
  }
  ngOnInit(): void {
  	// fetch data from service
  	this._apiservice.getItem(this.id).subscribe(res => {
  		console.log(res)
  		this.annotation = res;

      this._apiservice.getFragments(this.annotation.contentUrl).subscribe(res => {
        
        this.fragments = res.sort((a, b) => Number(a['selector'].value.split('=')[1].split(",")[1]) > Number(b['selector'].value.split('=')[1].split(",")[1]) ? 1 : Number(a['selector'].value.split('=')[1].split(",")[1]) === Number(b['selector'].value.split('=')[1].split(",")[1]) ? 0 : -1);
        this.renderFragment();
      });

  		this.setValues();

  	});
  }

}
