import { Component, OnInit } from '@angular/core';
import { AnthillService } from '../services/anthill.service';
@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  annotations: any =[];
  headerText: string = "file name or identifier";
  constructor(private _apiservice: AnthillService) { }

  ngOnInit(): void {
    this.getData()
  }
  getData() {
    // fetch data from service
    this._apiservice.getdata().subscribe(res => {
      this.annotations = res;

     
    });
  }
}
