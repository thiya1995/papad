import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  value: string = "";

  constructor(private _route: ActivatedRoute,
              private _router: Router) {
                this._route.paramMap.subscribe(params => {
                    
                    let paramKeys = Object.keys(params['params']);
                    paramKeys.forEach((key, i) => {   
                      if (i > 0){
                        this.value += paramKeys[i] +':' + params['params'][paramKeys[i]] + " ";
                      }
                      
                    })
                    
                  });
               }

  onEnter(e) {
  	var parts = e.target.value.split(' ');
  	var query = parts.map(function(part){
  		return part.split(':');//.join('=');
  	});
  	var queryObj = new Object;;
  	console.log(e, e.target.value, query, "entered");
  	if(query.length > 1) {
  		query[0][1] ? queryObj[query[0][0]] = query[0][1] : "";
  		query[1][1] ? queryObj[query[1][0]] = query[1][1] : "";

  		this._router.navigate(['/channels/all', 
          queryObj]);
  	} else {
  		query[0][1] ? queryObj[query[0][0]] = query[0][1] : "";
  		this._router.navigate(['/channels/all', 
          queryObj]);
  	}
  	
  }
  ngOnInit(): void {
  }

}
