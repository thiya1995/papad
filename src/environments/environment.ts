// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const base_url = 'https://maya-api.test.openrun.net/';
const station_url = 'https://maya-api.test.openrun.net/channels/';
const syncthin_url = 'http://sagehome:8384/';
const syncthing_api_key = 'UCghtfhjGsczJo9Q4EYcmqKuWioomtK6';
const nginx_file_index = 'https://maya-spano-files.test.openrun.net/';
const upload_endpoint = 'https://maya-spano.test.openrun.net/';

export const environment = {
  production: false,
  appName: 'Papad',
  logo: {
    imageUrl: '../assets/images/logo-transparent.png'
  },
  deviceId: 'Maya',
  channels: [],
  admin: 'Mirzapur Admin',
  syncthing: {
    url: syncthin_url,
    key: syncthing_api_key
  },
  files: nginx_file_index,
  uploadUrl: upload_endpoint,
  apiUrl: {
    upload: '',
    getAnnos: {
      base_getAnnos: base_url
    },
    postAnnos: {
      base_recordUrl: base_url
    },
    deleteAnnos: '',
    stations: {
      base_stationUrl: station_url
    },
    // users:{
    //   base_userUrl: user_url
    // },
    // records:{
    //   base_recordUrl: records_url
    // },
    version: 1
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
