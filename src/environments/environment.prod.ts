const base_url = 'https://papad-api-2.test.openrun.net/';
const station_url = 'https://papad-api-2.test.openrun.net/channels/';
const syncthin_url = 'http://192.168.83.168:8384/';
const syncthing_api_key = 'UCghtfhjGsczJo9Q4EYcmqKuWioomtK6';
const nginx_file_index = 'http://192.168.83.168/syncshare/';
const upload_endpoint = 'http://sagehome:5000';

export const environment = {
  production: true,
  appName: 'Papad',
  logo: {
    imageUrl: '../assets/images/logo-transparent.png'
  },
  deviceId: 'pi-recorded-1',
  channels: [],
  admin: 'SampleName',
  syncthing: {
    url: syncthin_url,
    key: syncthing_api_key
  },
  uploadUrl: upload_endpoint,
  apiUrl: {
    upload: '',
    getAnnos: {
      base_getAnnos: base_url
    },
    postAnnos: '',
    deleteAnnos: '',
    stations: {
      base_stationUrl: station_url
    },
    // users:{
    //   base_userUrl: user_url
    // },
    // records:{
    //   base_recordUrl: records_url
    // },
    version: 1
  }
};
